module github.com/issue9/mux/v6

require (
	github.com/issue9/assert/v2 v2.2.1
	github.com/issue9/errwrap v0.2.1
	github.com/issue9/sliceutil v0.10.1
)

go 1.18
